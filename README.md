# Hyenas Genomic Project Repository

Welcome to the Hyenas Genomic Project repository, where we provide access to the data and scripts associated with the Hyenas Genomic Project (part of https://hyena-project.com/).

Within this repository, you will find:

* SNP data acquired using the 3RADseq method for 284 spotted hyenas: '3RAD_Hyenas.vcf.gz'
* R script for conducting linear model analysis: 'LinearModelinR.R'

This dataset has been analyzed in the paper titled "Scaling-up RADseq methods for large datasets of non-invasive samples: lessons for library construction and data preprocessing," authored by Arantes et al. in 2023, which has been accepted for publication in Molecular Ecology Resources.


